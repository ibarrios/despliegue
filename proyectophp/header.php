
<html>
<header>
    <title>Pisos Pal Vicente</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/pricing.css" rel="stylesheet">


    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal">
            <?php if (isset($_SESSION['user']))  {
                if (isset($_SESSION['mensajeria'])) {echo "<blergh style='color: #7d0000'>" . $_SESSION['mensajeria'] . "</blergh>";unset($_SESSION['mensajeria']);} else {
                    echo "BIENVENIDO ".$_SESSION['user'];}
            } else {echo "Pisos Pal Vicente";}?>
            <?php if (isset($_SESSION['userAdmin']))  {
                if (isset($_SESSION['mensajeA'])) {echo "<br><blergh style='color: #7d0000'>" . $_SESSION['mensajeA'] . "</blergh>";unset($_SESSION['mensajeA']);} else {
                    echo "<br>BIENVENIDO ADMIN";}
            }?>

        <?php if(isset($_SESSION['user'])){usuariosConDatos();}?>
        </h5>

        <a class="navbar-brand mx-auto" ><div id="logo"><img width="200px" height="70px" style="border-radius: 10px" src="<?php echo mostrarLogo(); ?>"></div></a>

        <nav class="my-2 my-md-0 mr-md-3">
            <?php if (isset($_SESSION['user']) || isset($_SESSION['userAdmin'])){
                if (!verificarAdmin()){
                    echo "<a class='p-2 text-dark' href='perfil.php'>Perfil</a>";} else{
                    echo "<a class='p-2 text-dark' href='administracion.php'>Panel Administración</a>";
                }}?>
            <?php if (isset($_SESSION['user']) || isset($_SESSION['userAdmin'])){
                if (verificarAdmin()){
                    if (consultaCorreos("admin")) {
                        echo "Buzon: ".consultaCorreos("admin");
                    } else {
                        echo "Buzon: 0";
                    }
                } else {
                    if (consultaCorreos($_SESSION['user'])) {
                        echo "Buzon: ".consultaCorreos($_SESSION['user']);
                    } else {
                        echo "Buzon: 0";
                    }
                }
            }?>
            <?php if (isset($_SESSION['user']) || isset($_SESSION['userAdmin'])){
                if (verificarAdmin()){
                    echo "<a class='p-2 text-dark' href='gestorpisos.php'>Gestor de Pisos</a>";}}?>
            <?php if (isset($_SESSION['user'])){

                    if(!strpos($_SERVER['REQUEST_URI'],"index.php")){
                        echo "<a class='p-2 text-dark' href='index.php'>Volver al inicio</a>";}


            }?>

        </nav>

        <?php if (!isset($_SESSION['user'])){
            if (!verificarAdmin()){
                echo "<a class='btn btn-outline-primary' href='login.php'>Entrar</a>";
            } else {
                echo "<a class='btn btn-outline-primary' href='logout.php'>Salir de Admin</a>";
            }
        } else {
        if (!verificarAdmin()) {
            echo "<a class='btn btn-outline-primary' href='logout.php'>Salir</a>";
        } else {
            echo "<a class='btn btn-outline-primary' href='logout.php'>Salir de Admin</a>";

        }
        }

        ?>

    </div>

</header>

</html>
