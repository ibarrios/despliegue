<html>
<link href="css/styleform.css" rel="stylesheet">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <?php  session_start(); include'funciones.php';?>
    <?php
    if (isset($_SESSION['userAdmin'])){
        header('Location: logout.php');
    }?>
</head>
<div class="wrapper fadeInDown">
    <div id="formContent">

        <h3 style="color: #7d0000" ><b><?php mensaReg();?></b></h3>

        <form action="verificador.php" method="post">
            <input type="text" id="login" class="fadeIn second" name="user" placeholder="usuario">
            <input type="password" id="password" class="fadeIn third" name="pass" placeholder="Contraseña">
            <input type="submit" class="fadeIn fourth" value="registro" name="botonRegistro">
        </form>

        <div id="formFooter">
            <a class="underlineHover" href="login.php">Volver al login</a><br>
            <a class="underlineHover" href="index.php">Volver a la portada</a>
            <?php $_SESSION = null?>
        </div>

    </div>
</div>
<?php require_once 'footer.php';?>

</html>


