<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php
session_start();
include 'funciones.php';

include 'header.php';
if (!verificarAdmin()) {
    header('location: loginadmin.php');
};
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">

                <a href='administracion.php' class='list-group-item list-group-item-action '>Buzon</a>
                <a href='modificaradmin.php' class='list-group-item list-group-item-action'>Eliminar Usuario</a>
                <a href='crearadmin.php' class='list-group-item list-group-item-action active'>Crear Administrador</a>
                <a href='subirlogo.php' class='list-group-item list-group-item-action'>Cambiar Logo</a>



            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Crear nuevo admin</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="verificador.php" method="post">
                                <div class="form-group row">
                                    <label for="username" class="col-4 col-form-label">Introduce nuevo admin</label>
                                    <div class="col-8">
                                        <input id="newAdminUser" name="newAdminUser" placeholder="Introduce el usuario" class="form-control here"  type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce contraseña nuevo admin</label>
                                    <div class="col-8">
                                        <input  id="newAdminPass" name="newAdminPass" placeholder="Introduce la contraseña" class="form-control here" type="password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <input name="botonNuevoAdmin" value ="Cambiar" type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

</html>