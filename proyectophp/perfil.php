<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php

session_start();
include 'funciones.php';
verificadorActividad();
verificarLogado();
include 'header.php';
if (isset($_SESSION['userAdmin'])){
    header('Location: logout.php');
}
?>
<style>
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <a href="perfil.php" class="list-group-item list-group-item-action active">Datos</a>
                <a href="contrasena.php" class="list-group-item list-group-item-action">Contraseña</a>
                <a href='buzonuser.php' class='list-group-item list-group-item-action'>Buzon</a>
                <a href="favoritos.php" class="list-group-item list-group-item-action">Favoritos</a>
                <a href="borraruser.php" class="list-group-item list-group-item-action">Borrar Cuenta</a>



            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Tus Datos</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="verificador.php" method="post">
                                <div class="form-group row">
                                    <label for="username" class="col-4 col-form-label">Nombre</label>
                                    <div class="col-8">
                                        <input id="username" name="nombre" value="<?php if(isset($_SESSION['nombre'])) {echo $_SESSION['nombre']; }?>" placeholder="Introduce el nombre" class="form-control here"  type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Primer apellido</label>
                                    <div class="col-8">
                                        <input id="name" name="apellido1" value="<?php if(isset($_SESSION['apellido1'])) {echo $_SESSION['apellido1']; }?>" placeholder="Introduce el 1º apellido" class="form-control here" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lastname" class="col-4 col-form-label">Segundo apellido</label>
                                    <div class="col-8">
                                        <input id="lastname" name="apellido2" value="<?php if(isset($_SESSION['apellido2'])) {echo $_SESSION['apellido2']; }?>" placeholder="Introduce el 2º apellido" class="form-control here" type="text">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="text" class="col-4 col-form-label">Edad</label>
                                    <div class="col-8">
                                        <input id="number" style="-moz-appearance: textfield;" name="edad" value="<?php if(isset($_SESSION['edad'])) {echo $_SESSION['edad']; }?>" placeholder="Introduce tu edad" class="form-control here"  type="number">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-4 col-form-label">Email</label>
                                    <div class="col-8">
                                        <input id="email" name="email" placeholder="Introduce tu correo"value="<?php if(isset($_SESSION['email'])) {echo $_SESSION['email']; }?>" class="form-control here"  type="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="website" class="col-4 col-form-label">Telefono</label>
                                    <div class="col-8">
                                        <input id="tel" name="telefono" placeholder="Introduce tu Teléfono" style="-moz-appearance: textfield;" value="<?php if(isset($_SESSION['telefono'])) {echo $_SESSION['telefono']; }?>" class="form-control here" type="number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <input name="botonCambiarDatos" value ="Modificar" type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

</html>