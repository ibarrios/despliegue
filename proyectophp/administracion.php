<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php
session_start();
include 'funciones.php';
include 'header.php';
if (!verificarAdmin()) {
    header('location: loginadmin.php');
};
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">

                <a href='administracion.php' class='list-group-item list-group-item-action active'>Buzon</a>
                <a href='modificaradmin.php' class='list-group-item list-group-item-action'>Eliminar Usuario</a>
                <a href='crearadmin.php' class='list-group-item list-group-item-action'>Crear Administrador</a>
                <a href='subirlogo.php' class='list-group-item list-group-item-action'>Cambiar Logo</a>



            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Buzon</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            SIN LEER
                            <table border="2" align="center">
                                <ol style="list-style: none">
                                    <?php listadoCorreos(0,"admin");?>
                                </ol>
                            </table>


                            LEIDOS
                            <table border="2" align="center">
                                <ol style="list-style: none">
                                    <?php listadoCorreos(1,"admin");?>
                                </ol>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

</html>