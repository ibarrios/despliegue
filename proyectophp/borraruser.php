<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php

session_start();
include 'funciones.php';
verificadorActividad();
verificarLogado();
include 'header.php';
if (isset($_SESSION['userAdmin'])){
    header('Location: logout.php');
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <a href="perfil.php" class="list-group-item list-group-item-action">Datos</a>
                <a href="contrasena.php" class="list-group-item list-group-item-action">Contraseña</a>
                <a href='buzonuser.php' class='list-group-item list-group-item-action'>Buzon</a>
                <a href="favoritos.php" class="list-group-item list-group-item-action">Favoritos</a>
                <a href="borraruser.php" class="list-group-item list-group-item-action active">Borrar Cuenta</a>




            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Borrar Cuenta</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="verificador.php" method="post">

                                <div class="form-group row">
                                    <label for="name" class="col-4 col-form-label">Introduce tu contraseña para borrar tu cuenta</label>
                                    <div class="col-8">

                                        <input id="name" id="passNew1" name="passBorrar" placeholder="Introduce contraseña" class="form-control here" type="password">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="offset-4 col-8">
                                        <input style="background-color: red" name="botonBorrarUser" value ="Borrar" type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

</html>