<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php
session_start();
include 'funciones.php';

if (!verificarAdmin()) {
    header('location: loginadmin.php');
};include 'header.php';

?>
<style>
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        margin: 0;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <a href="gestorpisos.php" class="list-group-item list-group-item-action active ">Añadir Piso</a>
                <a href="modificarpiso.php" class="list-group-item list-group-item-action ">Modificar Piso</a>
                <a href="eliminarpiso.php" class="list-group-item list-group-item-action  ">Eliminar Piso</a>




            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Añadir Piso</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" action="verificador.php" method="post" >
                                <input type ="text" id= "nombrePiso" name="nombrePiso"  placeholder="Nombre del piso" >
                                <br>
                                <input type ="number" style="-moz-appearance: textfield;" id= "habitacionesPiso" name="habitacionesPiso"  placeholder="Habitaciones del Piso">
                                <br>
                                <input type ="number" style="-moz-appearance: textfield;"id= "precioPiso" name="precioPiso"  placeholder="Precio del piso">
                                <br>
                                <input type ="number" style="-moz-appearance: textfield;"id= "distanciaPiso" name="distanciaPiso" placeholder="Distancia al montessori">
                                <br>
                                <input type ="number" style="-moz-appearance: textfield;"id= "telefonoPiso" name="telefonoPiso"  placeholder="Telefono del piso">
                                <br>
                                <input type ="text" readonly id= "laImagenNuevoPiso" name="laImagenNuevoPiso" value="<?php
                                if (isset($_SESSION['url'])){ echo $_SESSION['url'];}?>"  placeholder="Carga una imagen">
                                <br>
                                <textarea name="descripcionPiso" id="descripcionPiso"  placeholder="Introduce una descripcion del piso"></textarea><br>
                                <br><br>
                                <input type ="submit" class='btn btn-primary' name = "datosPiso" value ="Crear"><br>
                            </form>
                            <form name='' action='verificador.php' method='post' enctype='multipart/form-data'>
                                Cargar imagen
                                <tr><input id='imagen' name='imagen' size='30' type='file' /></tr>
                                <tr><input type='submit' class='btn btn-primary' name='imagenNuevoPiso' value='Modificar'></tr>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';unset($_SESSION['url'])?>

</html>