<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php

session_start();
include 'funciones.php';
verificadorActividad();
verificarLogado();
include 'header.php';
if (isset($_SESSION['userAdmin'])){
    header('Location: logout.php');
}
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <a href="perfil.php" class="list-group-item list-group-item-action">Datos</a>
                <a href="contrasena.php" class="list-group-item list-group-item-action">Contraseña</a>
                <a href='buzonuser.php' class='list-group-item list-group-item-action'>Buzon</a>
                <a href="favoritos.php" class="list-group-item list-group-item-action active">Favoritos</a>
                <a href="borraruser.php" class="list-group-item list-group-item-action">Borrar Cuenta</a>





            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Favoritos</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php consultaFavs($_SESSION['user'])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

</html>