<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php
session_start();
include 'funciones.php';

include 'header.php';
if (!verificarAdmin()) {
    header('location: loginadmin.php');
};
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <a href='administracion.php' class='list-group-item list-group-item-action '>Buzon</a>
                <a href='modificaradmin.php' class='list-group-item list-group-item-action active'>Eliminar Usuario</a>
                <a href='crearadmin.php' class='list-group-item list-group-item-action'>Crear Administrador</a>
                <a href='subirLogo.php' class='list-group-item list-group-item-action'>Cambiar Logo</a>




            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Eliminar Usuario</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table border="2" align="center">
                                <ol style="list-style: none">
                                    <?php listadoUsers(); ?>
                                </ol>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

</html>