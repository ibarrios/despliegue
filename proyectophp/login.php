<html>
<link href="css/styleform.css" rel="stylesheet">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php  session_start(); $_SESSION['user']=null;$_SESSION['pass']=null; include "funciones.php";?>

    <?php
    if (isset($_SESSION['userAdmin'])){
        header('Location: logout.php');
    }?>
</head>
<div class="wrapper fadeInDown">
    <div id="formContent">

        <h3  ><b><?php mensaLog();?></b></h3>

        <form action="verificador.php" method="post">
            <input type="text" id="login" class="fadeIn second" name="user" placeholder="usuario">
            <input type="password" id="password" class="fadeIn third" name="pass" placeholder="Contraseña">
            <input type="submit" class="fadeIn fourth" value="entrar" name = "botonEntrar">
        </form>

        <div id="formFooter">
            <a class="underlineHover" href="registro.php">Deseas registrarte?</a><br>
            <a class="underlineHover" href="index.php">Volver a la portada</a>

            <?php $_SESSION['mensajeria'] = null?>
            <?php $_SESSION['mensaje'] = null?>
            <?php $_SESSION['mensajeA'] = null?>
        </div>

    </div>
</div>

</html>


