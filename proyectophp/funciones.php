<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<?php

function verificadorActividad() {
    if (isset($_SESSION['user'])){
        if (isset($_COOKIE['miCookie'])) {
            setcookie("miCookie",$_SESSION['user'],time()+300);
        } else {
            header('location:logout.php');
        }
    }
}

function crearCookie(){

    setcookie("miCookie",$_SESSION['user'],time()+300);
    header('location: index.php');
}

function accesoBBDD() {
    $mysqli = new mysqli('localhost','root','','ppv');
    if (!$mysqli) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $mysqli;
}

function registraUser() {
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("INSERT INTO usuario (user, pass) VALUES (?, ?)");
    $stmt->bind_param('ss', $_SESSION['user'], $_SESSION['pass']);
    $stmt->execute();
    $result = $stmt->close();
    $_SESSION['mensaje']= "Usuario creado<br>";
    header('location: login.php');
}

function existeUser() {
    $mysqli = accesoBBDD();
    $queryUser = "SELECT * FROM usuario where user = '".$_SESSION['user']."'";
    $resultadoUser = $mysqli->query($queryUser);
    $existeUser = $resultadoUser->num_rows;
    if ($existeUser == 1 ){
        $_SESSION['mensajeREG'] = "El nombre introducido ya existe en la base de datos<br>";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {
        return true;
    }
}

function existeUserPass() {
    $mysqli = accesoBBDD();
    $queryPass = "SELECT * FROM usuario where user = '".$_SESSION['user']."' AND pass = '".$_SESSION['pass']."' ";
    $resultadoPass = $mysqli->query($queryPass);
    $existePass = $resultadoPass->num_rows;
    if ($existePass == 1) {
        return true;
    } else {
        $_SESSION['mensaje']= "Usuario o contraseña incorrecto<br>";
        $_SESSION['user'] = null;
        header('Location:' . getenv('HTTP_REFERER'));
    }
}

function verificarFormReg () {
    if ($_SESSION['user'] == "") {
        $_SESSION['mensajeREG']= "No has introducido un Nombre<br>";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if (strlen($_SESSION['pass']) < 6) {
        $_SESSION['mensajeREG'] = "La contraseña debe ser de mínimo 6 caracteres<br>";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {
        return true;
    }
}

function verificarFormLog(){
    if ($_SESSION['user'] =="") {
        $_SESSION['mensaje']= "No has introducido un nombre<br>";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if (strlen($_SESSION['pass']=="")) {
        $_SESSION['mensaje']= "No has introducido contraseña<br>";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {
        return true;
    }
}

function mensaLog() {
    if (!empty($_SESSION['mensaje'])) {
        echo $_SESSION['mensaje'];
    } else {
        echo "<span style='color:black;'>Iniciar Sesión</span>";
    }
}

function mensaReg() {
    if (!empty($_SESSION['mensajeREG'])) {
        echo $_SESSION['mensajeREG'];
    }else {
        echo "<span style='color:black;'>Regístrate</span>";
    }
}


function verificarLogado() {
    if (!isset($_SESSION['user'])){
        echo $_SESSION['mensaje'] = "Debes logarte para ver el contenido";
        header('location: login.php');
    }
}


function usuariosConDatos (){
    $mysqli = accesoBBDD();
    $query = "SELECT * FROM usuario where user = '".$_SESSION['user']."'";
    $resultado = $mysqli->query($query);
    $datos = mysqli_fetch_assoc($resultado);
    $_SESSION['nombre'] = $datos['nombre'];
    $_SESSION['apellido1'] = $datos['apellido1'];
    $_SESSION['apellido2'] = $datos['apellido2'];
    $_SESSION['telefono'] = $datos['telefono'];
    $_SESSION['email'] = $datos['email'];
    $_SESSION['edad'] = $datos['edad'];
    $_SESSION['id'] = $datos['id'];
    return $datos;
}


function cambiarPass() {
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare(" update usuario set pass = ? where user = ?");
    $stmt->bind_param('ss',$_SESSION['passNew2'],$_SESSION['user']);
    $stmt->execute();
    $stmt->close();
    echo $_SESSION['mensaje'] = "Contraseña Cambiada";
    header('Location:login.php');
}

function verificarNuevaPass() {
    if($_SESSION['passOld'] != $_SESSION['pass']) {
        echo $_SESSION['mensajeria'] = "Contraseña vieja incorrecta";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if (strlen($_SESSION['passNew1']) < 6) {
        echo $_SESSION['mensajeria'] = "Contraseña inferioa a 6 caracteres";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if ($_SESSION['passNew1']!= $_SESSION['passNew2']) {
        echo $_SESSION['mensajeria'] = "Las contraseñas nuevas no coinciden";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {
        return true;
    }
}

function insertarDato($query){
    $mysqli = accesoBBDD();
    $mysqli->query($query);
    $mysqli->close();
    $_SESSION['mensajeA'] = "Datos modificados";
    $_SESSION['mensajeria'] = "Datos modificados";
    header('Location:' . getenv('HTTP_REFERER'));
}



function verificarAdmin(){
    if (isset($_SESSION['userAdmin'])){

        $mysqli = accesoBBDD();
        $resultado = $mysqli->query ("select * from admin where user = '".$_SESSION['userAdmin']."' and pass = '".$_SESSION['passAdmin']."' ");
        $numerico = $resultado -> num_rows;
        if ($numerico == 1) {
            return true ;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function listadoUsers(){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM usuario");
    $datos = mysqli_fetch_all($resultado);

    foreach ($datos as $daticos) {
        if($daticos[0] == "pepe") {} else  {echo "<form action='verificador.php' method='post'>";
            echo "<li style='padding-top: 10px'><input readonly type='text' name='elUser' 
                    id='elUser' value='$daticos[0]'></li><br>";
            echo "<input type='submit' name='botonBorrarUserAdmin' id='borrar'  class='btn btn-primary' value='Borrar'> ";

            echo "</form>";}
    }
}



function enviarMensajeBuzon($mensaje,$envia,$recibe){

    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("INSERT INTO buzon (mensaje, envia,recibe) VALUES (?,?,?)");
    $stmt->bind_param('sss',$mensaje, $envia,$recibe);
    $stmt->execute();
    $stmt->close();
    header('Location:' . getenv('HTTP_REFERER'));
}

function listadoCorreos($leido,$user) {
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM buzon");
    $datos = mysqli_fetch_all($resultado);

    foreach ($datos as $daticos) {
        if ($daticos[3]==$leido){
            if($daticos[4] == $user){echo "<form action='verificador.php' method='post'>";
                if($daticos[4] == "admin"){echo "<li style='padding-top: 10px'><input readonly type='text' name='elUser' 
                    id='elUser' value='$daticos[0]'></li><br>";}
                echo "<li style='padding-top: 10px'><textarea readonly name='elMensaje' 
                    id='elMensaje'>$daticos[1]</textarea></li><br>";
                if($daticos[4] == "admin"){if ($daticos[3] == 0) {echo "<input type='submit' class='btn btn-primary'name='botonMarcarLeido' value='Leido'>";
                } else { echo "<input type='submit' class='btn btn-primary' name='botonMarcarNoLeido' value='Marcar como No leido'>";}}
                echo "<input type='submit' name='botonBorrarMensaje' class='btn btn-primary' value='Borrar'>";
                if($daticos[4] == "admin"){echo "<input type='submit' class='btn btn-primary'name='botonResponder' value='Responder'>";
                    if($daticos[4] == "admin"){echo "<li style='padding-top: 10px'><textarea name='laRespuesta' id='elMensaje' 
                        placeholder='Escribe aquí la respuesta para $daticos[0]'></textarea></li><br>";}}
                echo "<input type='hidden' name='idDelMensaje' value='$daticos[2]'>";
                echo "</form>";}}
    }
}

function modificarLeido($leido,$id){
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("update buzon set leido = ? where id = ?");
    $stmt->bind_param('is',$leido, $id);
    $stmt->execute();
    $stmt->close();
    header('Location:' . getenv('HTTP_REFERER'));
}

function borrarMensaje($id){
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("delete from buzon where id = ?");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $stmt->close();
    header('Location:' . getenv('HTTP_REFERER'));
}

function subirImagen($nombre,$ruta){
    verificarLogo();
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("INSERT INTO imagen (nombre, ruta) VALUES (?, ?)");
    $stmt->bind_param('ss', $nombre, $ruta);
    $stmt->execute();
   $stmt->close();
}

function verificarLogo(){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM imagen");
    $datos = mysqli_fetch_all($resultado);
    foreach ($datos as $daticos) {
        if ($daticos[0] == logo){
            $mysqli->query("delete from imagen where nombre = 'logo'");
        }
    }

}
function mostrarLogo(){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM imagen");
    $datos = mysqli_fetch_all($resultado);
    foreach ($datos as $daticos) {
        if ($daticos[0] == "logo"){
            return $daticos[1];
        }
    }
}

function consultaCarrusel ($idPiso){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM imagen where id_piso = '".$idPiso."'");
    $datos = mysqli_fetch_all($resultado);
    $totalArticulos = $resultado->num_rows;
    if ($totalArticulos >= 1) {
        return $datos;
        return true;
    } else {
        return false;
    }
}


$mysqli = accesoBBDD();
$tbl_name = "pisos";
$pagina = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
$postPorPagina = 0;
if (isset($_SESSION)){
    if (isset($_SESSION['user'])){
        $postPorPagina = 1;
    } else {
        $postPorPagina = 5;
    }
} else {
    $postPorPagina = 5;
}

$inicio = ($pagina > 1) ? ($pagina * $postPorPagina - $postPorPagina) : 0 ;
$query = "SELECT * FROM $tbl_name";
$result = $mysqli->query($query);
$totalArticulos = $result->num_rows;
$query = "SELECT SQL_CALC_FOUND_ROWS * FROM $tbl_name LIMIT $inicio, $postPorPagina";
$numeroPaginas = ceil($totalArticulos / $postPorPagina);

function mostrarPiso(){
    $mysqli = accesoBBDD();
    global $query;
    $resultado = $mysqli->query($query);
    $datos = mysqli_fetch_all($resultado);

    foreach ($datos as $daticos){
        if (empty($_SESSION)){
            echo "<img width='400px'   style='border-radius: 10px;margin-bottom: 20px'  src='".$daticos['7']."'>";
        } else {
            if (consultaCarrusel($daticos[0])) {
                carrusel($daticos['0']);
            } else {
                echo "<img width='400px'  style='border-radius: 10px;margin-bottom: 20px'  src='".$daticos['7']."'>";}
            }
        echo "<br><input   readonly type='text' value='Titulo: ".$daticos['1']."'><br>";
        echo "<br><input  readonly type='text' value='Telefono: ".$daticos['6']."'><input  readonly type='text' value='Habitaciones: ".$daticos['2']."'><br>";
        echo "<br><input style='margin-bottom: 20px'  readonly type='text' value='Precio: ".$daticos['3']."'><input  readonly type='text' value='Colegio: ".$daticos['5']."'><br>";


        if (!empty($_SESSION)){echo "<textarea readonly>".$daticos['4']."</textarea>";
            echo "<form action='verificador.php' method='post'>";
            if (!existeFav($daticos['0'],$_SESSION['id'])) {
                echo "<input type='checkbox'  name='pisoFavorito' onchange='this.form.submit()' value='fav'>Favorito";
            } else {
                echo "<input type='checkbox'  name='quitarFav' onchange='this.form.submit()' value='fav' checked>Favorito";
            };
            echo "<input type='submit' class='btn btn-primary' name ='imprimirDatos' value='Imprimir'>";
            echo "<input type='hidden' name ='idDelPiso' value='$daticos[0]'>";
            echo "</form>";
        }
    }
}

function imprimirPiso($id){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM pisos where id='".$id."' ");
    $datos = mysqli_fetch_array($resultado);
    $imagenCarrusel = $mysqli->query("SELECT * FROM imagen where id_piso='".$id."' ");
    $datosCarrusel = mysqli_fetch_all($imagenCarrusel);


    ob_start();
    $s=92;
    require('fpdf/fpdf.php');
    $pdf = new FPDF();
    $pdf -> AddPage();
    $pdf -> SetFont('Arial', 'B', 14);
    $pdf->Image($datos['7'], 100, 32, 55, 58, 'JPG');
    foreach ($datosCarrusel as $elCarrusel){
        $pdf->Image($elCarrusel[1], 100, $s, 55, 58, 'JPG');
        $s=$s+62;

    }
    $pdf ->Ln();
    $pdf -> Cell(40,10,iconv('UTF-8', 'windows-1252',"Titulo: ".$datos['1']));
    $pdf ->Ln();
    $pdf -> Cell(40,10,iconv('UTF-8','windows-1252',"Número de habitaciones: ".$datos['2']));
    $pdf ->Ln();
    $pdf -> Cell(40,10,iconv('UTF-8','windows-1252',"Precio: ".$datos['3']));
    $pdf ->Ln();
    $pdf -> Cell(40,10,iconv('UTF-8','windows-1252',"Distancia del colegio: ".$datos['5']));
    $pdf ->Ln();
    $pdf -> Cell(40,10,iconv('UTF-8','windows-1252',"Teléfono: ".$datos['6']));
    $pdf ->Ln();
    $pdf -> Cell(40,10,iconv('UTF-8','windows-1252',"DESCRIPCION"));
    $pdf ->Ln();
    $pdf -> Cell(40,10,iconv('UTF-8','windows-1252',$datos['4']));
    $pdf->Output();
}


function nuevoPiso($titulo,$habitaciones,$precio,$descripcion,$distancia,$telefono,$imagen) {
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("INSERT INTO pisos (titulo,habitaciones,precio,descripcion,distancia,telefono,imagen) 
        VALUES (?,?,?,?,?,?,?)");
    $stmt->bind_param('siisiis', $titulo,$habitaciones,$precio,$descripcion,$distancia,$telefono,$imagen);
    $stmt->execute();
    $stmt->close();
    $_SESSION['mensajeA']= "Piso creado<br>";
    header('Location:' . getenv('HTTP_REFERER'));

    ;
}
function listadoNombresPisos(){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM pisos");
    $datos = mysqli_fetch_all($resultado);
    foreach ($datos as $daticos) {
        echo "<form action='verificador.php' method='post'>";
        echo "<input type='text' readonly name = 'elPiso' value='$daticos[1]'>";
        echo "<input type='hidden' name='elId' value='$daticos[0]'>";
        echo "<input type='submit' class='btn btn-primary' name='borrarPiso' value='Borrar'>";
        echo "</form>";
    }
}


function borrarPiso($id){
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("delete from pisos where id = ?");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $stmt->close();
    $stmt = $mysqli->prepare("delete from likes where idPiso = ?");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $stmt->close();
    $stmt = $mysqli->prepare("delete from imagen where id_piso = ?");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $stmt->close();
    $_SESSION['mensajeA']= "Piso borrado<br>";
    header('Location:' . getenv('HTTP_REFERER'));
}

function borrarUser($user){
    $mysqli = accesoBBDD();

    $stmt = $mysqli->prepare("delete from usuario where user = ?");
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $stmt->close();
    $stmt = $mysqli->prepare("delete from buzon where envia = ? || recibe = ?");
    $stmt->bind_param('ss', $user,$user);
    $stmt->execute();
    $stmt->close();
    header('Location:' . getenv('HTTP_REFERER'));
}

function editarPiso()
{
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM pisos");
    $datos = mysqli_fetch_all($resultado);
    foreach ($datos as $daticos) {
        $_SESSION['nombrePiso'] = $daticos[1];
        $_SESSION['habitacionesPiso'] = $daticos[2];
        $_SESSION['precioPiso'] = $daticos[3];
        $_SESSION['descripcionPiso'] = $daticos[4];
        $_SESSION['distanciaPiso'] = $daticos[5];
        $_SESSION['telefonoPiso'] = $daticos[6];
        $_SESSION['elId'] = $daticos[0];
        echo "<form name='' action='verificador.php' method='post' >";
        echo "<table>";
        echo "<tr><td>Titulo:</td><td> <input type ='text'  name='nombrePiso' value='$daticos[1]'></td></tr>";
        echo "<tr><td>Habitaciones:</td><td> <input type ='number'style='-moz-appearance: textfield;' name='habitacionesPiso' value='$daticos[2]'></td></tr>";
        echo "<tr><td>Precio:</td><td> <input type ='number' style='-moz-appearance: textfield;' name='precioPiso' value = '$daticos[3]'></td></tr>";
        echo "<tr><td>Distancia:</td><td> <input type ='number' style='-moz-appearance: textfield;' name='distanciaPiso' value='$daticos[5]'></td></tr>";
        echo "<tr><td>Teléfono:</td><td> <input type ='number'  style='-moz-appearance: textfield' name='telefonoPiso'  value='$daticos[6]'></td></tr>";
        echo "<tr><td>Descripcion:</td><td> <textarea name ='descripcionPiso'  placeholder='Descripcion del piso'>$daticos[4]</textarea></td></tr>";
        echo "<input type='hidden' name='elId' value='$daticos[0]'>";
        echo "<tr><td><input type ='submit'class='btn btn-primary' name = 'botonCambiarDatosPiso' value ='Modificar'></td></tr>";
        echo "</table>";
        echo "</form>";
        echo "<tr><b>Imagen Principal:</b> $daticos[7] </tr>";
        echo "<form name='' action='verificador.php' method='post' enctype='multipart/form-data'>";
        echo "<tr><input id='imagen' name='imagen' size='30' type='file' /></tr>";
        echo "<input type='hidden' name='elId' value='$daticos[0]'>";
        echo "<tr><input type='submit' class='btn btn-primary' name='imagenPiso' value='Modificar'></tr>";
        echo "</form>";
        $carrusel = consultaCarrusel($daticos[0]);
        echo "<b>Carrusel:</b><ul>";
        if (!empty($carrusel)){
        foreach ( $carrusel as $datosCarrusel){
            echo "<li>$datosCarrusel[1]</li>";
        }}
        echo "</ul>";
        echo "<form name='' action='verificador.php' method='post' enctype='multipart/form-data'>";
        echo "<input type='hidden' name='elId' value='$daticos[0]'>";
        echo "<tr><input id='imagen' name='imagen' size='30' type='file' /></tr>";
        echo "<tr><input type='submit' class='btn btn-primary' name='imagenCarruselPiso' value='Modificar'></tr>";
        echo "</form>";

    }
}



function subirImagenPiso($imagen,$id){
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("update pisos set imagen = ? where id = ? ");
    $stmt->bind_param('ss', $imagen, $id);
    $stmt->execute();
    $result = $stmt->close();



}

function subirImagenCarruselPiso($imagen,$id){
    $mysqli = accesoBBDD();
    $stmt = $mysqli->prepare("INSERT INTO imagen (ruta,id_piso) 
        VALUES (?,?)");
    $stmt->bind_param('si', $imagen, $id);
    $stmt->execute();
    $result = $stmt->close();


}

function consultaCorreos($user){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query("SELECT * FROM buzon where leido = '0' and recibe = '".$user."'");
     $numerico = $resultado->num_rows;

     if ($numerico == 0) {
         return false ;
     } else {
         return $numerico;
     }

}

function consultaFavs($user){
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query ("SELECT * FROM `pisos` inner join likes on likes.idPiso = pisos.id inner join usuario 
                on likes.idUser = usuario.id where usuario.user = '".$user."'");
    $datos = mysqli_fetch_all($resultado);

    foreach ($datos as $daticos) {
        echo "<img height='400px' style='border-radius: 10px' src='".$daticos['7']."'>";
        echo "<table style=''>";
        echo "<tr><td>Telefono: ".$daticos['6']."</td></tr>";
        echo "<tr><td>Titulo: ".$daticos['1']."</td><td>Habitaciones: ".$daticos['2']."</td></tr>";
        echo "<tr><td>Precio: ".$daticos['3']."</td><td>Colegio: ".$daticos['5']."</td></tr>";
        echo "</table>";

    }
}

function existeFav($idPiso,$idUser) {
    $mysqli = accesoBBDD();
    $resultado = $mysqli->query ("select * from likes where idPiso = '".$idPiso."' and idUser = '".$idUser."' ");

    $numerico = $resultado -> num_rows;

    if ($numerico == 1) {
        return true ;
    } else {
        return false;
    }
}


$mysqli = accesoBBDD();
$resultset = $mysqli->query("SELECT * FROM imagen where id_piso = '21' ");

$image_count = 0;
$slider_html = '';
while( $rows = mysqli_fetch_assoc($resultset)){

    $active_class = "";
    if(!$image_count) {
        $active_class = 'active';
        $image_count = 1;
    }
    $image_count++;
    $thumb_image = "nature_thumb_".$rows['id'].".jpg";
    $slider_html.= "<div class='item ".$active_class."'>";
    $slider_html.= "<img src='".$rows['ruta']."' alt='1.jpg' class='img-responsive'>";
    $slider_html.= "<div class='carousel-caption'></div></div>";

}
function carrusel($id)
{

    global $slider_html;
    $mysqli = accesoBBDD();
    $resultset = $mysqli->query("SELECT * FROM imagen where id_piso = '" . $id . "' ");

    $image_count = 0;
    $slider_html = '';
    while ($rows = mysqli_fetch_assoc($resultset)) {

        $active_class = "";
        if (!$image_count) {
            $active_class = 'active';
            $image_count = 1;
        }
        $image_count++;
        $slider_html .= "<div class='item " . $active_class . "'>";
        $slider_html .= "<img   height='400px' style='border-radius: 10px' src='" . $rows['ruta'] . "' alt='1.jpg' class='img-responsive'>";
        $slider_html .= "<div class='carousel-caption'></div></div>";

    }
        echo "<div class='container'>";
        echo "<div id='carousel-example-generic' class='carousel slide' data-ride='carousel' data-interval='false'>";

        echo " <div class='carousel-inner'>";
        echo $slider_html;
        echo " </div>";
        echo " <a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'>";
        echo "<span class='glyphicon glyphicon-chevron-left'></span>";
        echo " </a>";
        echo "<a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'>";
        echo "<span class='glyphicon glyphicon-chevron-right'></span>";
        echo " </a>";
        echo "</div>";
        echo "</div>";
}