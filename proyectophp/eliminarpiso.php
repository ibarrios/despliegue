<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<html><?php
session_start();
include 'funciones.php';

if (!verificarAdmin()) {
    header('location: loginadmin.php');
};include 'header.php';
?>

<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <a href="gestorpisos.php" class="list-group-item list-group-item-action  ">Añadir Piso</a>
                <a href="modificarpiso.php" class="list-group-item list-group-item-action ">Modificar Piso</a>
                <a href="eliminarpiso.php" class="list-group-item list-group-item-action active ">Eliminar Piso</a>




            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Eliminar Piso</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php listadoNombresPisos();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php';?>

</html>