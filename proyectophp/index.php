<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
<link href="css/style.css" rel="stylesheet">
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'></script>
<script src="js/carousel-slider.js"></script>


<html>

<?php

session_start();
include 'funciones.php';
verificadorActividad();
require 'header.php';

if (isset($_SESSION['userAdmin'])){
    header('Location: logout.php');
}
?>




<body>

<div class="container">
    <div class="row">
        <div class="col-md-3 ">
            <div class="list-group ">
                <?php
                for($i = 1; $i <= $numeroPaginas; $i++){
                    if ($pagina === $i) {
                        echo "<a class='list-group-item list-group-item-action active ' href='?pagina=$i'>$i</a>";
                    } else {
                        echo "<a href='?pagina=$i'>$i</a>";
                    }
                }
                ?>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Catálogo</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php mostrarPiso()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>

<?php require_once 'footer.php';?>
</html>
