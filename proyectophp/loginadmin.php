<html>
<link href="css/styleform.css" rel="stylesheet">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php
    session_start();
    session_destroy();
    session_start();

    include 'funciones.php';


    ?>
</head>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <h3><b>ADMINISTRACION</b></h3>
        <h3  ><b><?php if (isset($_SESSION['mensajeA'])) echo $_SESSION['mensajeA']?></b></h3>

        <form action="verificador.php" method="post">
            <input type="text" id="login" class="fadeIn second" name="userAdmin" placeholder="usuario">
            <input type="password" id="password" class="fadeIn third" name="passAdmin" placeholder="Contraseña">
            <input type="submit" class="fadeIn fourth" value="entrar" name = "botonEntrarAdmin">
        </form>
        <a class="underlineHover" href="index.php">Volver a la portada</a>

        <?php $_SESSION['mensajeA'] = null?>
    </div>
</div>

</html>
