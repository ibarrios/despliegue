<?php
session_start();
include "funciones.php";





if (isset($_POST['botonRegistro'])) {
    $_SESSION['user']= $_POST['user'];
    $_SESSION['pass']= $_POST['pass'];
    $_POST['registro'] = null;
    if (verificarFormReg()) {
        if (existeUser()) {
            registraUser();
        }
    }
}
if (isset($_POST['botonEntrar'])){
    $_SESSION['user']= $_POST['user'];
    $_SESSION['pass']= $_POST['pass'];
    $_POST['botonEntrar'] = null;
    if (verificarFormLog()) {
        if (existeUserPass()) {
            crearCookie();
        }
    }
}

if (isset($_POST['botonCambiarPass'])) {
    $_POST['botonCambiarPass'] = null;
    $_SESSION['passOld'] = $_POST['passOld'];
    $_SESSION['passNew1'] = $_POST['passNew1'];
    $_SESSION['passNew2'] = $_POST['passNew2'];
    if (verificarNuevaPass()) {
        cambiarPass();
    }
}


if (isset($_POST['botonCambiarDatos'])) {
    $_POST['botonCambiarDatos'] = null;
    if ($_POST['nombre']!= $_SESSION['nombre'] ) {
        $query = "update usuario set nombre = '" . $_POST['nombre'] . "' where user = '" . $_SESSION['user'] . "'";
        insertarDato($query);
    }

    if ($_POST['apellido1']!= $_SESSION['apellido1']) {
        $query = "update usuario set apellido1 = '" . $_POST['apellido1'] . "' where user = '" . $_SESSION['user'] . "'";
        insertarDato($query);
    }

    if ($_POST['apellido2']!= $_SESSION['apellido2']) {
        $query = "update usuario set apellido2 = '" . $_POST['apellido2'] . "' where user = '" . $_SESSION['user'] . "'";
        insertarDato($query);
    }

    if ($_POST['telefono']!= $_SESSION['telefono']) {
        $query = "update usuario set telefono = '" . $_POST['telefono'] . "' where user = '" . $_SESSION['user'] . "'";
        insertarDato($query);
    }

    if ($_POST['email']!= $_SESSION['email']) {
        $query = "update usuario set email = '" . $_POST['email'] . "' where user = '" . $_SESSION['user'] . "'";
        insertarDato($query);
    }

    if ($_POST['edad']!= $_SESSION['edad']) {
        $query = "update usuario set edad = '" . $_POST['edad'] . "' where user = '" . $_SESSION['user'] . "'";
        insertarDato($query);
    }
    if (empty($_SESSION['mensajeria'])){
        $_SESSION['mensajeria'] = "Introduce algún dato";
        header('location: perfil.php');
    }
}

if (isset($_POST['botonBorrarUser'])) {
    $_POST['botonBorrarUser'] = null;
    if ($_POST['passBorrar'] != $_SESSION['pass']){
        $_SESSION['mensajeria'] = "Contraseña incorrecta";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {
        borrarUser($_SESSION['user']);
        $_SESSION['mensaje']= "Usuario borrado<br>";
        session_destroy();
        header('Location: index.php');

    }
}

if (isset($_POST['botonBorrarUserAdmin'])) {
    $_POST['botonBorrarUserAdmin']=null;
    borrarUser($_POST['elUser']);
    $_SESSION['mensajeA'] = "USUARIO BORRADO";
}


if (isset($_POST['botonBuzonPerfil'])) {
    $_POST['botonBuzonPerfil']=null;
    if ($_POST['paraBuzonAdmin'] !="") {
        enviarMensajeBuzon($_POST['paraBuzonAdmin'],$_SESSION['user'],"admin");
        $_SESSION['mensajeria'] = "Mensaje enviado";
    } else {
        $_SESSION['mensajeria'] = " No has cargado ningún mensaje";
        header('Location:' . getenv('HTTP_REFERER'));
    }
}

if (isset($_POST['botonMarcarLeido'])) {
    $_POST['botonMarcarLeido'] = null;
    modificarLeido(1,$_POST['idDelMensaje']);
    $_SESSION['mensajeA'] = "Has cambiado a Leido";
}

if (isset($_POST['botonMarcarNoLeido'])) {
    $_POST['botonMarcarNoLeido'] = null;
    modificarLeido(0,$_POST['idDelMensaje']);
    $_SESSION['mensajeA'] = "Has cambiado a No Leido";

}

if (isset($_POST['botonBorrarMensaje'])) {
    $_POST['botonBorrarMensaje'] = null;
    borrarMensaje($_POST['idDelMensaje']);
    $_SESSION['mensajeria'] = "Has Borrado el mensaje";
    $_SESSION['mensajeA'] = "Has Borrado el mensaje";

}

if (isset($_POST['botonResponder'])) {
    $_POST['botonResponder'] =null;
    if ($_POST['laRespuesta'] !="") {
        enviarMensajeBuzon($_POST['laRespuesta'],"admin",$_POST['elUser']);
        $_SESSION['mensajeria'] = "Mensaje enviado";
        $_SESSION['mensajeA'] = "Mensaje enviado";

    } else {
        $_SESSION['mensajeria'] = " No has cargado ninguna respuesta";
        $_SESSION['mensajeA'] =  "No has cargado ninguna respuesta";
        header('Location:' . getenv('HTTP_REFERER'));
    }
}

if (isset($_POST['elLogo'])) {
    $tamano = $_FILES['imagen']['size'];
    $nombre_img = $_FILES['imagen']['name'];
    $directorio = './imagenes/';

    $formatos = array('gif', 'png', 'jpg');
    $ext = pathinfo($nombre_img, PATHINFO_EXTENSION);

    if ($tamano == 0){
        $_SESSION['mensajeA'] = "No has subido ninguna imagen";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if (!in_array($ext, $formatos)) {
		header('Location:' . getenv('HTTP_REFERER'));
		$_SESSION['mensajeA'] = "Fichero no soportado";
    } else {
        move_uploaded_file($_FILES['imagen']['tmp_name'],$_SERVER['/imagenes'].$directorio.$nombre_img);
        $_SESSION['url']= $directorio.$nombre_img;
        subirImagen("logo",$_SESSION['url']);
        header('Location:' . getenv('HTTP_REFERER'));
        $_SESSION['mensajeA'] = "Logo cambiado";

    }
}



if (isset($_POST['datosPiso'])) {
    $_POST['datosPiso'] = null;
    $contador=0;
    if (($_POST['nombrePiso']) ==""){
        $_SESSION['mensajeA'] = "No has rellenado todos los datos";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {$contador++;}
    if (($_POST['habitacionesPiso']) ==""){
        $_SESSION['mensajeA'] = "No has rellenado todos los datos";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {$contador++;}
    if (($_POST['precioPiso']) ==""){
        $_SESSION['mensajeA'] = "No has rellenado todos los datos";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {$contador++;}
    if (($_POST['distanciaPiso']) ==""){
        $_SESSION['mensajeA'] = "No has rellenado todos los datos";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {$contador++;}
    if (($_POST['telefonoPiso']) ==""){
        $_SESSION['mensajeA'] = "No has rellenado todos los datos";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {$contador++;}
    if (($_POST['descripcionPiso']) ==""){
        $_SESSION['mensajeA'] = "No has rellenado todos los datos";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {$contador++;}
    if (($_POST['laImagenNuevoPiso']) ==""){
        $_SESSION['mensajeA'] = "No has rellenado todos los datos";
        header('Location:' . getenv('HTTP_REFERER'));
    } else {$contador++;}
    if ($contador == 7) {
        nuevoPiso($_POST['nombrePiso'],$_POST['habitacionesPiso'],$_POST['precioPiso'],
            $_POST['descripcionPiso'],$_POST['distanciaPiso'],$_POST['telefonoPiso'],$_POST['laImagenNuevoPiso']);
        $contador = 0;
    }
}

if (isset($_POST['borrarPiso'])){
    borrarPiso($_POST['elId']);

}


if (isset($_POST['botonCambiarDatosPiso'])) {
    if ($_POST['nombrePiso']!= $_SESSION['nombrePiso'] ) {
        $query = "update pisos set titulo = '" . $_POST['nombrePiso'] . "' where id = '" . $_POST['elId'] . "'";
        insertarDato($query);
    }

    if ($_POST['habitacionesPiso']!= $_SESSION['habitacionesPiso']) {
        $query = "update pisos set habitaciones = '" . $_POST['habitacionesPiso'] . "' where id = '" . $_POST['elId'] . "'";
        insertarDato($query);
    }

    if ($_POST['precioPiso']!= $_SESSION['precioPiso']) {
        $query = "update pisos set precio = '" . $_POST['precioPiso'] . "' where id = '" . $_POST['elId'] . "'";
        insertarDato($query);
    }

    if ($_POST['descripcionPiso']!= $_SESSION['descripcionPiso']) {
        $query = "update pisos set descripcion = '" . $_POST['descripcionPiso'] . "' where id = '" . $_POST['elId'] . "'";
        insertarDato($query);
    }

    if ($_POST['distanciaPiso']!= $_SESSION['distanciaPiso']) {
        $query = "update pisos set distancia = '" . $_POST['distanciaPiso'] . "' where id = '" . $_POST['elId'] . "'";
        insertarDato($query);
    }

    if ($_POST['telefonoPiso']!= $_SESSION['telefonoPiso']) {
        $query = "update pisos set telefono = '" . $_POST['telefonoPiso'] . "' where id = '" . $_POST['elId'] . "'";
        insertarDato($query);
    }

    if (empty($_SESSION['mensajeA'])){
        echo $_SESSION['mensajeA'] = "Introduce algún dato";
        header('Location:' . getenv('HTTP_REFERER'));
    }

}
if (isset($_POST['imagenPiso'])) {
    $tamano = $_FILES['imagen']['size'];
    $nombre_img = $_FILES['imagen']['name'];
    $directorio = './imagenes/';

    $formatos = array('gif', 'png', 'jpg');
    $ext = pathinfo($nombre_img, PATHINFO_EXTENSION);

    if ($tamano == 0){
        $_SESSION['mensajeA'] = "No has subido ninguna imagen";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if (!in_array($ext, $formatos)) {
        		header('Location:' . getenv('HTTP_REFERER'));
		$_SESSION['mensajeA'] = "Fichero no soportado";
    } else {
        move_uploaded_file($_FILES['imagen']['tmp_name'],$_SERVER['/imagenes'].$directorio.$nombre_img);
        $_SESSION['url']= $directorio.$nombre_img;
        subirImagenPiso($_SESSION['url'],$_POST['elId']);
        $_SESSION['mensajeA'] = "Imagen principal modificada";
        header('Location:' . getenv('HTTP_REFERER'));
        $_SESSION['url'] = null;    }
}

if (isset($_POST['imagenNuevoPiso'])) {
    $tamano = $_FILES['imagen']['size'];
    $nombre_img = $_FILES['imagen']['name'];
    $directorio = './imagenes/';
    $formatos = array('gif', 'png', 'jpg');
    $ext = pathinfo($nombre_img, PATHINFO_EXTENSION);
    if ($tamano == 0){
        $_SESSION['mensajeA'] = "No has subido ninguna imagen";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if (!in_array($ext, $formatos)) {
        		header('Location:' . getenv('HTTP_REFERER'));
		$_SESSION['mensajeA'] = "Fichero no soportado";
    } else {
        move_uploaded_file($_FILES['imagen']['tmp_name'],$_SERVER['/imagenes'].$directorio.$nombre_img);
        $_SESSION['url']= $directorio.$nombre_img;

        header('Location:' . getenv('HTTP_REFERER'));
    }
}


if (isset($_POST['idDelPiso'])) {
    if (isset($_POST['imprimirDatos'])){
        $_POST['imprimirDatos'] =null;
        imprimirPiso($_POST['idDelPiso']);
        print_r($_POST);

    } else {
        if (isset($_POST['pisoFavorito'])){
            $mysqli = accesoBBDD();
            $stmt = $mysqli->prepare("INSERT INTO likes (idPiso,idUser) 
        VALUES (?,?)");
            $stmt->bind_param('ii', $_POST['idDelPiso'],$_SESSION['id']);
            $stmt->execute();

            $stmt->close();
            header('Location:' . getenv('HTTP_REFERER'));
            $_SESSION['mensajeria'] = "Tienes un nuevo favorito";
        } else {
            $mysqli = accesoBBDD();
            $stmt = $mysqli->prepare("delete from likes where idPiso = ? && idUser =?");
            $stmt->bind_param('ii', $_POST['idDelPiso'],$_SESSION['id']);
            $stmt->execute();
            $stmt->close();
            header('Location:' . getenv('HTTP_REFERER'));
            $_SESSION['mensajeria'] = "Has quitado el favorito";
        }
    }
}

if (isset($_POST['botonEntrarAdmin'])) {
    $_SESSION['userAdmin'] = $_POST['userAdmin'];
    $_SESSION['passAdmin'] = $_POST['passAdmin'];
    if (verificarAdmin()){
        header('Location: administracion.php');
    } else {
        echo $_SESSION['mensajeA'] = "Usuario y contraseña incorrectos";
        header('Location:' . getenv('HTTP_REFERER'));

    }

}

if (isset($_POST['botonNuevoAdmin'])){
        if ($_POST['newAdminUser'] == "") {
            $_SESSION['mensajeA']= "No has introducido un usuario<br>";
            header('Location:' . getenv('HTTP_REFERER'));
        } else if (($_POST['newAdminPass']) =="") {
            $_SESSION['mensajeA'] = "No has introducido contraseña<br>";
            header('Location:' . getenv('HTTP_REFERER'));
        } else {
            $mysqli = accesoBBDD();
            $stmt = $mysqli->prepare("INSERT INTO admin (user, pass) VALUES (?,?)");
            $stmt->bind_param('ss',$_POST['newAdminUser'], $_POST['newAdminPass']);
            $stmt->execute();
            $stmt->close();
            echo $_SESSION['mensajeA'] = "Nuevo Administrador Creado";
            header('Location:' . getenv('HTTP_REFERER'));        }
    }

if (isset($_POST['imagenCarruselPiso'])){
    $tamano = $_FILES['imagen']['size'];
    $nombre_img = $_FILES['imagen']['name'];
    $directorio = './imagenes/';

    $formatos = array('gif', 'png', 'jpg');
    $ext = pathinfo($nombre_img, PATHINFO_EXTENSION);

    if ($tamano == 0){
        $_SESSION['mensajeA'] = "No has subido ninguna imagen";
        header('Location:' . getenv('HTTP_REFERER'));
    } else if (!in_array($ext, $formatos)) {
                		header('Location:' . getenv('HTTP_REFERER'));
		$_SESSION['mensajeA'] = "Fichero no soportado";
    } else {
        move_uploaded_file($_FILES['imagen']['tmp_name'],$_SERVER['/imagenes'].$directorio.$nombre_img);
        $_SESSION['url']= $directorio.$nombre_img;
        subirImagenCarruselPiso($_SESSION['url'],$_POST['elId']);
        echo $_SESSION['mensajeA'] = "Nuevo imagen en carrusel";

        header('Location:' . getenv('HTTP_REFERER'));

    }
}


?>






