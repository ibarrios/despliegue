-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2020 at 06:44 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ppv`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user`, `pass`, `id`) VALUES
('pepe', 'pepe', 1),
('lol', 'lele', 2),
('antonio', 'lalala', 3);

-- --------------------------------------------------------

--
-- Table structure for table `buzon`
--

CREATE TABLE `buzon` (
  `envia` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `mensaje` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `id` int(19) NOT NULL,
  `leido` tinyint(1) NOT NULL,
  `recibe` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `buzon`
--

INSERT INTO `buzon` (`envia`, `mensaje`, `id`, `leido`, `recibe`) VALUES
('raquel', 'fghfgh', 40, 1, 'admin'),
('admin', 'asdasdas', 41, 0, 'raquel');

-- --------------------------------------------------------

--
-- Table structure for table `imagen`
--

CREATE TABLE `imagen` (
  `nombre` varchar(20) NOT NULL,
  `ruta` varchar(100) NOT NULL,
  `id` int(3) NOT NULL,
  `id_piso` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `imagen`
--

INSERT INTO `imagen` (`nombre`, `ruta`, `id`, `id_piso`) VALUES
('logo', './imagenes/ss (2020-11-04 at.jpg', 16, 0),
('', './imagenes/1 (1).jpeg', 40, 23),
('', './imagenes/1 (22).jpg', 41, 23),
('', './imagenes/1 (23).jpg', 42, 23),
('', './imagenes/1 (17).jpg', 43, 25),
('', './imagenes/1 (18).jpg', 44, 25);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `idPiso` int(11) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `idPiso`, `idUser`) VALUES
(32, 36, 24),
(33, 41, 24);

-- --------------------------------------------------------

--
-- Table structure for table `pisos`
--

CREATE TABLE `pisos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `habitaciones` varchar(20) NOT NULL,
  `precio` int(10) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `distancia` int(6) NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `id_imagen` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pisos`
--

INSERT INTO `pisos` (`id`, `titulo`, `habitaciones`, `precio`, `descripcion`, `distancia`, `telefono`, `imagen`, `id_imagen`) VALUES
(23, 'Piso 3', '345', 4, '', 65, '', './imagenes/1 (1).jpeg', NULL),
(24, 'Piso 4', '3', 3, 'dfssdf', 3, '650409983', './imagenes/1 (1).jpg', NULL),
(25, 'Piso 5', '3', 1, 'dfssdf', 23, '650409983', './imagenes/1 (2).jpg', NULL),
(26, 'piso 6', '4', 546, 'dsfdsf', 41100, '65', './imagenes/1 (3).jpg', NULL),
(27, 'piso 7', '3', 67, 'trytrytr', 56, '456', './imagenes/1 (4).jpg', NULL),
(28, 'piso 8', '234', 345345, 'dfgdfgdfg', 456, '2352', './imagenes/1 (5).jpg', NULL),
(29, 'piso 9', '6', 56, 'rgvsdfds', 34, '345', './imagenes/1 (6).jpg', NULL),
(30, 'piso 10', '65', 546, 'rtyrty', 546, '456', './imagenes/1 (7).jpg', NULL),
(31, 'piso 11', '4', 345, 'tertertert', 234, '234', './imagenes/1 (8).jpg', NULL),
(32, 'piso 11', '3', 567, 'ghjghj', 567, '567', './imagenes/1 (9).jpg', NULL),
(33, 'piso 12', '2', 23, 'sfsdf', 23, '650', './imagenes/1 (10).jpg', NULL),
(34, 'piso 13', '1', 1, 'srgsf', 1, '1', './imagenes/1 (11).jpg', NULL),
(35, 'piso 14', '3', 3, 'sdfdsf', 3, '3', './imagenes/1 (12).jpg', NULL),
(36, 'Piso 16', '1', 1, 'asdasd', 1, '1', './imagenes/1 (13).jpg', NULL),
(37, 'Piso 17', '3', 3, 'sdfsdf', 3, '3', './imagenes/1 (14).jpg', NULL),
(38, 'piso 18', '1', 1, 'adasd', 1, '1', './imagenes/1 (15).jpg', NULL),
(39, 'piso 15', '1', 1, 'adfdsf', 1, '1', './imagenes/1 (16).jpg', NULL),
(40, 'Piso 19', '', 0, '', 0, '', './imagenes/1 (17).jpg', NULL),
(41, 'Piso 20', '', 0, '', 0, '', './imagenes/1 (18).jpg', NULL),
(42, 'Piso 19', '1', 1, '1', 1, '1', './imagenes/1 (19).jpg', NULL),
(43, 'Piso 20', '1', 1, '1', 1, '1', './imagenes/1 (20).jpg', NULL),
(44, 'Piso 22', '1', 12000, 'werwerwer', 10000, '1', './imagenes/1 (21).jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `user` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `pass` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `nombre` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido1` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido2` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `edad` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`user`, `pass`, `admin`, `nombre`, `apellido1`, `apellido2`, `telefono`, `email`, `edad`, `id`) VALUES
('raquel', 'lalala', 0, 'raquel', 'rtyrtyrt', 'rtyrt', '', '', '', 24),
('nacho', 'lalala', 0, '', '', '', '', '', '', 26);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buzon`
--
ALTER TABLE `buzon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagen`
--
ALTER TABLE `imagen`
  ADD PRIMARY KEY (`id`,`id_piso`) USING BTREE,
  ADD KEY `id_piso` (`id_piso`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`,`idPiso`,`idUser`),
  ADD KEY `idPiso` (`idPiso`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_imagen` (`id_imagen`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `buzon`
--
ALTER TABLE `buzon`
  MODIFY `id` int(19) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `imagen`
--
ALTER TABLE `imagen`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `pisos`
--
ALTER TABLE `pisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`idPiso`) REFERENCES `pisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
