-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 14, 2020 at 02:37 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ppv`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `id` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`user`, `pass`, `id`) VALUES
('pepe', 'pepe', 1),
('lol', 'lele', 2),
('antonio', 'lalala', 3);

-- --------------------------------------------------------

--
-- Table structure for table `buzon`
--

CREATE TABLE `buzon` (
  `envia` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `mensaje` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `id` int(19) NOT NULL,
  `leido` tinyint(1) NOT NULL,
  `recibe` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `buzon`
--

INSERT INTO `buzon` (`envia`, `mensaje`, `id`, `leido`, `recibe`) VALUES
('raquel', 'fghfgh', 40, 0, 'admin'),
('admin', 'asdasdas', 41, 0, 'raquel'),
('admin', 'dsfsdfsd', 42, 0, 'raquel');

-- --------------------------------------------------------

--
-- Table structure for table `imagen`
--

CREATE TABLE `imagen` (
  `nombre` varchar(20) NOT NULL,
  `ruta` varchar(100) NOT NULL,
  `id` int(3) NOT NULL,
  `id_piso` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `imagen`
--

INSERT INTO `imagen` (`nombre`, `ruta`, `id`, `id_piso`) VALUES
('', './imagenes/1 (1).jpeg', 40, 23),
('', './imagenes/1 (22).jpg', 41, 23),
('', './imagenes/1 (23).jpg', 42, 23),
('', './imagenes/1 (17).jpg', 43, 25),
('', './imagenes/1 (18).jpg', 44, 25),
('logo', './imagenes/ss (2020-11-04 at.jpg', 49, 0);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `idPiso` int(11) NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `idPiso`, `idUser`) VALUES
(32, 36, 24),
(33, 41, 24);

-- --------------------------------------------------------

--
-- Table structure for table `pisos`
--

CREATE TABLE `pisos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `habitaciones` varchar(20) NOT NULL,
  `precio` int(20) NOT NULL,
  `descripcion` varchar(400) NOT NULL,
  `distancia` int(20) NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `imagen` varchar(100) NOT NULL,
  `id_imagen` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pisos`
--

INSERT INTO `pisos` (`id`, `titulo`, `habitaciones`, `precio`, `descripcion`, `distancia`, `telefono`, `imagen`, `id_imagen`) VALUES
(23, 'Gran Via 36', '5', 1200000, 'Este es el piso de la Gran Via', 65, '666555444', './imagenes/1 (1).jpeg', NULL),
(24, 'Calle Mayor 28', '3', 354000, 'Este es el piso de la calle mayor', 3, '650409983', './imagenes/1 (1).jpg', NULL),
(25, 'Piso 5', '3', 456445, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 23, '650409983', './imagenes/1 (2).jpg', NULL),
(26, 'piso 6', '4', 54612, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 2, '650409984', './imagenes/1 (3).jpg', NULL),
(27, 'piso 7', '3', 67334, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 56, '650409980', './imagenes/1 (4).jpg', NULL),
(28, 'piso 8', '9', 345345, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 456, '650208883', './imagenes/1 (5).jpg', NULL),
(29, 'piso 9', '6', 56452, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 64, '650009983', './imagenes/1 (6).jpg', NULL),
(30, 'piso 10', '9', 546123, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 546, '650409983', './imagenes/1 (7).jpg', NULL),
(31, 'piso 11', '4', 345452, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 234, '650401183', './imagenes/1 (8).jpg', NULL),
(32, 'piso 11', '3', 567753, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 567, '650123145', './imagenes/1 (9).jpg', NULL),
(33, 'piso 12', '2', 23523, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 23, '650456123', './imagenes/1 (10).jpg', NULL),
(34, 'piso 13', '1', 15347, 'srgsf\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in cul', 1123, '650407773', './imagenes/1 (11).jpg', NULL),
(35, 'piso 14', '3', 37412, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qu', 3753, '640009983', './imagenes/1 (12).jpg', NULL),
(36, 'Piso 16', '1', 18521, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 117, '650400983', './imagenes/1 (13).jpg', NULL),
(37, 'Piso 17', '3', 312312, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 3, '650479983', './imagenes/1 (14).jpg', NULL),
(38, 'piso 18', '5', 1753, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 112, '640409983', './imagenes/1 (15).jpg', NULL),
(39, 'piso 15', '1', 1789, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 142, '651409983', './imagenes/1 (16).jpg', NULL),
(40, 'Piso 19', '5', 434, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 435, '654504483', './imagenes/1 (17).jpg', NULL),
(41, 'Piso 20', '6', 456, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 20, '650209983', './imagenes/1 (18).jpg', NULL),
(42, 'Piso 19', '11', 4523, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 145, '650459983', './imagenes/1 (19).jpg', NULL),
(43, 'Piso 20', '10', 1123, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 123, '650309983', './imagenes/1 (20).jpg', NULL),
(44, 'Piso 22', '9', 12000, '\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ', 10000, '650407983', './imagenes/1 (21).jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `user` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `pass` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `nombre` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellido1` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `apellido2` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(9) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `edad` varchar(2) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`user`, `pass`, `admin`, `nombre`, `apellido1`, `apellido2`, `telefono`, `email`, `edad`, `id`) VALUES
('raquel', 'lalala', 0, 'raquel', 'rtyrtyrt', 'rtyrt', '', '', '', 24),
('nacho', 'lalala', 0, '', '', '', '', '', '', 26),
('ubuntu', 'lalala', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 28);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buzon`
--
ALTER TABLE `buzon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagen`
--
ALTER TABLE `imagen`
  ADD PRIMARY KEY (`id`,`id_piso`) USING BTREE,
  ADD KEY `id_piso` (`id_piso`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`,`idPiso`,`idUser`),
  ADD KEY `idPiso` (`idPiso`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_imagen` (`id_imagen`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `buzon`
--
ALTER TABLE `buzon`
  MODIFY `id` int(19) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `imagen`
--
ALTER TABLE `imagen`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `pisos`
--
ALTER TABLE `pisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `likes`
--
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`idPiso`) REFERENCES `pisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`idUser`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
